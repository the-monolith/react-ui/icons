import { component, React, Style } from 'local/react/component'
import { stylesheet } from 'local/react/head'

export const defaultIconStyle: Style = {}

export const Icon = component
  .props<{ style?: Style; brand?: boolean; name: string }>({
    brand: false,
    style: defaultIconStyle
  })
  .render(({ name, style, brand }) => {
    stylesheet('https://use.fontawesome.com/releases/v5.6.3/css/all.css', {
      integrity:
        'sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/',
      crossorigin: 'anonymous'
    })

    return <i style={style} className={`${brand ? 'fab' : 'fas'} fa-${name}`} />
  })
