# icons

## Usage

To use with FontAwesome brand icons:
`<Icon brand={true} name="facebook" />`

To use with FontAwesome solid icons:
`<Icon name="envelope-square" />`
